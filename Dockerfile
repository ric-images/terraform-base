FROM ricardocg94/tf-base:latest

# Common variables | versions from binaries 
ARG AWSCLI_VERSION=1.18.90

# Set AWS ARGS 
ARG AWS_KEY
ARG AWS_SECRET_KEY
ARG AWS_REGION 

# metadata of image
LABEL maintainer="Ricardocg <ricguerrero94@gmail.com>"
LABEL aws_cli_version=${AWSCLI_VERSION}

ENV DEBIAN_FRONTEND=noninteractive
ENV AWSCLI_VERSION=${AWSCLI_VERSION}

RUN pip3 install --upgrade awscli==${AWSCLI_VERSION} 

RUN aws configure set aws_access_key_id ${AWS_KEY} \
    && aws configure set aws_secret_access_key ${AWS_SECRET_KEY} \
    && aws configure set default.region ${AWS_REGION} 

#Install OPA
RUN curl -L -o opa https://openpolicyagent.org/downloads/latest/opa_linux_amd64 \
    && chmod 755 ./opa \
    && mv opa /usr/local/bin/ 
# Add boto3 module 
RUN pip3 install boto3

#Copy OPA policies
COPY scripts /opt/policies

CMD    ["/bin/bash"]